# About The Project

This README file contains detailed information about the Active Tech Infrastructure Code base. 

### Technologies Used

This section lists the technologies and tools used to build this Infrastructure.

- **Terraform**

This project uses the Terraform IAC tool to create resources in the AWS cloud platform.
The code base is divided into 2 parts:

1. The Modules directory where all the tiny parts of the application configuration definitions live e.g networking (vpc, subnets, internet gateway, kubernetes clusters e.t.c).

2. The Infrastructure directory where real live infrastructures are defined that leverages the modules defined in the Modules directory to create the resources such as networking, runners, EKS cluster, Kubernetes application and S3 storage to host the Terraform states.

*Best practices were used or applied in the creation of this infrastructure code including splitting tiny parts of the infrastructure into separate modules and applying or composing these modules to build the infrastructure resources. Also the Terraform state files are not stored locally but in a remote location (AWS S3) for security and collaboration purposes.*

### Side Note

This code base is used by the pipeline configuration defined in the https://gitlab.com/abdul-razaq/active_tech repository to create the infrastructure resources. Therefore, please refer to the README.md documentation of this repository for a detailed guideline.

*Thank you :)*

### Built By
**AbdulRazaq Suleiman Ayomide** as part of an **Active Tech** Interview assessment for the **DevOps Engineer role.**