terraform {
  backend "s3" {
    bucket = "active-tech-terraform-state"
    key = "global/eks_cluster/terraform.tfstate"
    region = "us-east-2"
    dynamodb_table = "terraform-state-locks"
    encrypt = true
  }
}

provider "aws" {
  region = "us-east-2"

  default_tags {
    tags = {
      Name = "Active Tech EKS Cluster"
      ManagedBy = "Terraform"
      Purpose = "Host Active Tech Application"
    }
  }
}

module "eks_cluster" {
  source = "../../../modules/services/eks_cluster"

  name = "active-tech"
  min_size = 1
  max_size = 2
  desired_size = 2

  instance_types = [ "t3.small" ]
} 
