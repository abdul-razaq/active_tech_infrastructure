output "cluster_name" {
  description = "The EKS cluster name"
  value = module.eks_cluster.cluster_name
}

output "cluster_arn" {
  description = "The ARN of the EKS cluster"
  value = module.eks_cluster.cluster_arn
}

output "cluster_endpoint" {
  description = "The EKS cluster endpoint"
  value = module.eks_cluster.cluster_endpoint
}

output "cluster_certificate_authority" {
  description = "The Certificate Authority of the EKS cluster"
  value = module.eks_cluster.cluster_certificate_authority
}
