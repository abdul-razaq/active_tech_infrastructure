data "aws_ami" "amazon_linux" {
  most_recent = true

  filter {
    name = "name"
    values = [ "amzn2-ami-kernel-5.10-hvm-2.0.20230926.0-x86_64-gp2" ]
  }

  owners = [ "137112412989" ]
}

data "aws_ami" "ubuntu" {
  most_recent = true

  owners = [ "099720109477" ]

  filter {
    name = "name"
    values = [ "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*" ]
  }
}

data "aws_iam_policy_document" "instance_assume_role_policy" {
  version = "2012-10-17"

  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [ "ec2.amazonaws.com" ]
    }  
  }
}