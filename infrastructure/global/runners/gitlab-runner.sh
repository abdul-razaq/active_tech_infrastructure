#!/bin/bash
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

sudo chmod +x /usr/local/bin/gitlab-runner

sudo /usr/local/bin/gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

sudo /usr/local/bin/gitlab-runner start

sudo /usr/local/bin/gitlab-runner register --non-interactive --url https://gitlab.com/ --registration-token ${registration_token} --executor "docker" --docker-image golang:1.18 --description "docker-runner" --tag-list "ec2,test,docker" --docker-volumes="/var/run/docker.sock:/var/run/docker.sock" --docker-volumes="/cache"

sudo /usr/local/bin/gitlab-runner register --non-interactive --url https://gitlab.com/ --registration-token ${registration_token} --executor "shell" --description "shell-build-runner" --tag-list "ec2,build,shell"

sudo /usr/local/bin/gitlab-runner register --non-interactive --url https://gitlab.com/ --registration-token ${registration_token} --executor "shell" --description "shell-deploy-runner" --tag-list "ec2,deploy,shell"

sudo /usr/local/bin/gitlab-runner restart

sudo /usr/local/bin/gitlab-runner verify

sudo yum update -y && sudo yum install git -y && sudo yum install docker -y

sudo systemctl start docker && sudo systemctl enable docker

sudo usermod -aG docker $(whoami) && sudo usermod -aG docker gitlab-runner && sudo systemctl restart docker

sudo reboot

sudo /usr/local/bin/gitlab-runner restart

sudo /usr/local/bin/gitlab-runner verify
