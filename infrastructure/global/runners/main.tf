terraform {
  backend "s3" {
    key = "global/runners/terraform.tfstate"
    bucket = "active-tech-terraform-state"
    region = "us-east-2"
    dynamodb_table = "terraform-state-locks"
    encrypt = true
  }
}

provider "aws" {
  region = "us-east-2"

  default_tags {
    tags = {
      Name = "Runner Instances"
      ManagedBy = "Terraform"
      Purpose = "Host Gitlab Runners"
    }
  }
}

module "networking" {
  source = "../../../modules/networking"
}

locals {
  from_port = 0
  to_port = 65535
  ip_protocol = "tcp"
  any_ip = "0.0.0.0/0"
}

resource "aws_iam_role" "instance_role" {
  name = "instance-role"
  assume_role_policy = data.aws_iam_policy_document.instance_assume_role_policy.json
  managed_policy_arns = [ "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore" ]
}

resource "aws_iam_instance_profile" "instance_profile" {
  name = "instance-profile"
  role = aws_iam_role.instance_role.name
}

resource "aws_instance" "runner_instance" {
  ami = data.aws_ami.amazon_linux.image_id
  instance_type = "t2.micro"
  associate_public_ip_address = true
  subnet_id = module.networking.runners_subnet.id
  count = 1
  vpc_security_group_ids = [ aws_security_group.runners_security_group.id ]

  user_data_replace_on_change = true
  user_data = templatefile("${path.module}/gitlab-runner.sh", {
    registration_token = var.registration_token
  })

  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
}

resource "aws_security_group" "runners_security_group" {
  name = "runners-security-group"
  description = "Allow all inbound and outbound traffic on the runner instances"
  vpc_id = module.networking.vpc_id

  tags = {
    Name = "Runner Security Group"
  }
}

resource "aws_vpc_security_group_ingress_rule" "allow_all_inbound_traffic" {
  security_group_id = aws_security_group.runners_security_group.id

  cidr_ipv4 = local.any_ip
  from_port = local.from_port
  to_port = local.to_port
  ip_protocol = local.ip_protocol
}

resource "aws_vpc_security_group_egress_rule" "allow_all_outbound_traffic" {
  security_group_id = aws_security_group.runners_security_group.id

  cidr_ipv4 = local.any_ip
  from_port = local.from_port
  to_port = local.to_port
  ip_protocol = local.ip_protocol
}
