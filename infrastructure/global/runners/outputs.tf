output "runners_subnet" {
  description = "The subnet responsible for hosting runner instances"
  value = module.networking.runners_subnet
}

output "deployment_subnet" {
  description = "The subnet responsible for hosting the applications"
  value = module.networking.deployment_subnet
}

output "vpc_id" {
  description = "The VPC ID"
  value = module.networking.vpc_id
}
