variable "registration_token" {
  description = "The gitlab runner registration token"
  type = string
  sensitive = true
}
