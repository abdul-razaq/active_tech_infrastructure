output "terraform_state_arn" {
  description = "The ARN of the S3 bucket where the terraform state is stored"
  value = aws_s3_bucket.terraform_state.arn
}

output "dynamoDB_table" {
  description = "The name of the dynamoDB table that defines the terraform state locks"
  value = aws_dynamodb_table.terraform_locks.name
}
