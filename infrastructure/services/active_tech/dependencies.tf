data "terraform_remote_state" "eks_cluster" {
  backend = "s3"

  config = {
    bucket = "active-tech-terraform-state"
    key = "global/eks_cluster/terraform.tfstate"
    region = "us-east-2"
  }
}

data "aws_eks_cluster_auth" "cluster_auth_token" {
  name = data.terraform_remote_state.eks_cluster.outputs.cluster_name
}
