terraform {
  backend "s3" {
    bucket = "active-tech-terraform-state"
    key = "services/active_tech/terraform.tfstate"
    region = "us-east-2"
    dynamodb_table = "terraform-state-locks"
    encrypt = true
  }
}

provider "aws" {
  region = "us-east-2"
}

provider "kubernetes" {
  host = data.terraform_remote_state.eks_cluster.outputs.cluster_endpoint
  cluster_ca_certificate = base64decode(data.terraform_remote_state.eks_cluster.outputs.cluster_certificate_authority[0].data)
  token = data.aws_eks_cluster_auth.cluster_auth_token.token
}

module "active_tech_app" {
  source = "../../../modules/services/k8s_app"

  name = "active-tech"
  environment = var.environment
  image = var.image
  replicas = var.environment == "prod" ? 2 : 1
  container_port = var.container_port
  load_balancer_port = var.environment == "prod" ? 80 : 8080

  environment_variables = var.environment_variables
}
