output "service_endpoint" {
  description = "The service endpoint where we access the cluster"
  value = module.active_tech_app.service_endpoint
}
