variable "image" {
  description = "The image to use to launch the Pods"
  type = string
}

variable "container_port" {
  description = "The port that the container listens on"
  type = number
}

variable "environment_variables" {
  description = "The environment variables to pass to the container"
  type = map(string)
  default = {}
}

variable "environment" {
  description = "The environment this deployment is created for"
  type = string
  validation {
    condition = contains(["dev", "staging", "prod"], var.environment)
    error_message = "Valid values for var: environment are (dev, staging and prod)"
  }
}
