terraform {
  required_version = ">= 1.0.0, <= 2.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 5.17.0"
    }
  }
}

locals {
  any_ip = "0.0.0.0/0"
}

# VPC DEFINITION AND INTERNET GATEWAY ASSOCIATION
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "Active Tech VPC"
    Owner = "Active Tech"
    ManagedBy = "Terraform"
  }
}

resource "aws_internet_gateway" "internet_gateway" {
  tags = {
    Name = "Active Tech VPC Internet gateway"
  }
}

resource "aws_internet_gateway_attachment" "vpc_igw_attachment" {
  vpc_id = aws_vpc.vpc.id
  internet_gateway_id = aws_internet_gateway.internet_gateway.id
}

# SUBNET FOR RUNNERS
resource "aws_subnet" "runners_subnet" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = data.aws_availability_zones.availability_zones.names[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "Runners Subnet"
    Purpose = "House instances to be used for running gitlab runners"
  }
}

resource "aws_route_table" "runners_subnet_route_table" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table_association" "associate_route_table_with_runners_subnet" {
  route_table_id = aws_route_table.runners_subnet_route_table.id
  subnet_id = aws_subnet.runners_subnet.id
}

resource "aws_route" "route_runners_traffic_to_igw" {
  route_table_id = aws_route_table.runners_subnet_route_table.id
  destination_cidr_block = local.any_ip
  gateway_id = aws_internet_gateway.internet_gateway.id
  depends_on = [ aws_internet_gateway_attachment.vpc_igw_attachment ]
}

# SUBNET FOR DEPLOYMENTS
resource "aws_subnet" "deployment_subnet" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = data.aws_availability_zones.availability_zones.names[1]
  map_public_ip_on_launch = true

  tags = {
    Name = "Deployment Subnet"
    Purpose = "House instances to be used for running the applications"
  }
}

resource "aws_route_table" "deployment_subnet_route_table" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table_association" "associate_route_table_with_deployment_subnet" {
  route_table_id = aws_route_table.deployment_subnet_route_table.id
  subnet_id = aws_subnet.deployment_subnet.id
}

resource "aws_route" "route_deployment_traffic_to_igw" {
  route_table_id = aws_route_table.deployment_subnet_route_table.id
  destination_cidr_block = local.any_ip
  gateway_id = aws_internet_gateway.internet_gateway.id
  depends_on = [ aws_internet_gateway_attachment.vpc_igw_attachment ]
}
