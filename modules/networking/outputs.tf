output "runners_subnet" {
  description = "The subnet responsible for hosting runner instances"
  value = aws_subnet.runners_subnet
}

output "deployment_subnet" {
  description = "The subnet responsible for hosting the applications"
  value = aws_subnet.deployment_subnet
}

output "vpc_id" {
  description = "The VPC ID"
  value = aws_vpc.vpc.id
}
