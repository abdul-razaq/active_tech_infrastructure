data "terraform_remote_state" "networking" {
  backend = "s3"

  config = {
    bucket = "active-tech-terraform-state"
    key = "global/runners/terraform.tfstate"
    region = "us-east-2"
  }
}

data "aws_subnets" "vpc_subnets" {
  filter {
    name = "vpc-id"
    values = [ data.terraform_remote_state.networking.outputs.vpc_id ]
  }
}
