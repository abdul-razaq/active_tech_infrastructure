terraform {
  required_version = ">= 1.0.0, <= 2.0.0"

  required_providers {
    aws = {
      version = "~> 5.17.0"
      source = "hashicorp/aws"
    }
  }
}

data "aws_iam_policy_document" "cluster_assume_role" {
  version = "2012-10-17"

  statement {
    effect = "Allow"
    actions = [ "sts:AssumeRole" ]
    principals {
      type = "Service"
      identifiers = [ "eks.amazonaws.com" ]
    }
  }
}

resource "aws_iam_role" "cluster_role" {
  name = "${var.name}-cluster-role"
  assume_role_policy = data.aws_iam_policy_document.cluster_assume_role.json
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  role = aws_iam_role.cluster_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

resource "aws_eks_cluster" "eks_cluster" {
  name = "${var.name}-eks-cluster"
  role_arn = aws_iam_role.cluster_role.arn
  version = "1.28"

  vpc_config {
    subnet_ids = toset(data.aws_subnets.vpc_subnets.ids)
  }

  depends_on = [ 
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy
  ]
}

data "aws_iam_policy_document" "node_group_assume_role" {
  version = "2012-10-17"

  statement {
    effect = "Allow"
    actions = [ "sts:AssumeRole" ]
    principals {
      type = "Service"
      identifiers = [ "ec2.amazonaws.com" ]
    }
  }
}

resource "aws_iam_role" "node_group_role" {
  name = "${var.name}-node-group-role"
  assume_role_policy = data.aws_iam_policy_document.node_group_assume_role.json
}

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  role = aws_iam_role.node_group_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  role = aws_iam_role.node_group_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  role = aws_iam_role.node_group_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}

resource "aws_eks_node_group" "worker_nodes" {
  cluster_name = aws_eks_cluster.eks_cluster.name
  node_group_name = "${var.name}-worker-nodes"
  node_role_arn = aws_iam_role.node_group_role.arn
  subnet_ids = toset(data.aws_subnets.vpc_subnets.ids)
  
  instance_types = var.instance_types

  scaling_config {
    min_size = var.min_size
    max_size = var.max_size
    desired_size = var.desired_size
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
  ]
}
