output "cluster_name" {
  description = "The EKS cluster name"
  value = aws_eks_cluster.eks_cluster.name
}

output "cluster_arn" {
  description = "The ARN of the EKS cluster"
  value = aws_eks_cluster.eks_cluster.arn
}

output "cluster_endpoint" {
  description = "The EKS cluster endpoint"
  value = aws_eks_cluster.eks_cluster.endpoint
}

output "cluster_certificate_authority" {
  description = "The Certificate Authority of the EKS cluster"
  value = aws_eks_cluster.eks_cluster.certificate_authority
}
