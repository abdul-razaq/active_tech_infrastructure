variable "name" {
  description = "The name of the kubernetes resource"
  type = string
}

variable "instance_types" {
  description = "The types of EC2 instances to be run in the kubernetes worker nodes"
  type = list(string)
}

variable "min_size" {
  description = "The minimum number of nodes to have in the worker nodes of the kubernetes cluster"
  type = number
}

variable "max_size" {
  description = "The maximum number of nodes to have in the worker nodes of the kubernetes cluster"
  type = number
}

variable "desired_size" {
  description = "The desired number of nodes to have in the worker nodes of the kubernetes cluster"
  type = number
}
