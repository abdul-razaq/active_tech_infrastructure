variable "name" {
  description = "The name for resources created by this module"
  type = string
}

variable "replicas" {
  description = "The number of Pod replicas"
  type = number
}

variable "image" {
  description = "The image to use to launch the Pods"
  type = string
}

variable "container_port" {
  description = "The port that the container listens on"
  type = number
}

variable "environment_variables" {
  description = "The environment variables to pass to the container"
  type = map(string)
  default = {}
}

variable "load_balancer_port" {
  description = "The port the load balancer should listen on"
  type = number
  default = 80
}

variable "environment" {
  description = "The environment this deployment is created for"
  type = string
  validation {
    condition = contains(["dev", "staging", "prod"], var.environment)
    error_message = "Valid values for var: environment are (dev, staging and prod)"
  }
}
